#include <stdio.h>
#include <stdlib.h>

struct arvore {
    int dado;
    struct arvore* esquerda;
    struct arvore* direita;
};

int verificaArvoreBinariaDeBuscaUtil(struct arvore* arvore, int anterior)
{
    if (arvore) {
        if (!verificaArvoreBinariaDeBuscaUtil(arvore->esquerda, anterior))
            return 0;

        if (arvore->dado <= anterior)
            return 0;

        return verificaArvoreBinariaDeBuscaUtil(arvore->direita, arvore->dado);
    }
    return 1;
}

int verificaArvoreBinariaDeBusca(struct arvore* arvore)
{
    return verificaArvoreBinariaDeBuscaUtil(arvore, INT_MIN);
}

struct arvore* novoNo(int dado)
{
    struct arvore* arvore = (struct arvore*)malloc(sizeof(struct arvore));
    arvore->dado = dado;
    arvore->esquerda = NULL;
    arvore->direita = NULL;

    return arvore;
}

int main()
{
    struct arvore* a = novoNo(4);
    a->esquerda = novoNo(2);
    a->direita = novoNo(5);
    a->esquerda->esquerda = novoNo(1);
    a->esquerda->direita = novoNo(3);

    if(verificaArvoreBinariaDeBusca(a))
        printf("E uma arvore binaria de busca");
    else
        printf("Nao e uma arvore binaria de busca");

    return 0;
}
